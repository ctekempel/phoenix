# README #

### What is this repository for? ###

This project is aimed at integrating Azure Cognitive Services into Sitecore via a resusable framework, that in theory at least 
should allow you to integrate other modelling/machine learning tools.

The approach to integrate the Azure services takes a similar approach to commerce connect, in that it is front by a service 
provider, that in turn invokes pipelines that can be customised.

The name "Phoenix" takes inspiration from the Phoenix Foundation from MacGuyver. The idea being that it is a framwork the 
integrates a series of agents, that can then be bgrought together to solve problems.

### Current State ###

In its current state it is part way through wiring up the Item to Item and user based recommendations services. Once the 
recommendations service has been wired up, the other services will be considered/integrated.

Note that at the moment the consumption of the recommendations servcice is just through stub pages/agents in the Phoenix project.

### How do I get set up? ###

Firstly will need to set up your own cognitive services account. Once you have that you can update the secret key in the Phoenix.Foundation/App_Config/Include/Phoenix.config file.

From there it is just a matter of updating the "Phoenix" website projects publishing profile to publish to you Sitecore instance.
