﻿namespace Phoenix.Agents
{
    using System.Linq;
    using System.Web;

    using Phoenix.Recommendations.Model;
    using Phoenix.Recommendations.ServiceProvider;

    public class TrainItemToItemAgent
    {
        #region Fields

        private readonly string catalogPath;

        private readonly string modelName;

        private readonly RecommendationServiceProvider recommendationServiceProvider;

        #endregion

        #region Constructors and Destructors

        public TrainItemToItemAgent(string modelName, string catalogPath)
        {
            this.modelName = modelName;
            this.catalogPath = catalogPath;
            this.recommendationServiceProvider = new RecommendationServiceProvider();
        }

        #endregion

        #region Public Methods and Operators

        public void Run()
        {
            var modelList = this.GetExistingModels();
            var model = modelList.Models.FirstOrDefault(info => info.Name == this.modelName) ?? this.CreateModel();

            this.UploadCatalog(model);
            this.UploadUsage(model);
            this.TrainModel(model);
        }

        #endregion

        #region Methods

        private ModelInfo CreateModel()
        {
            var createModelResult =
                this.recommendationServiceProvider.CreateModel(new CreateModelRequest { ModelName = this.modelName });
            if (!createModelResult.Success)
            {
                global::Sitecore.Diagnostics.Log.Error("Could not create model", createModelResult.Exception, this);
            }

            return createModelResult.Model;
        }

        private ModelInfoList GetExistingModels()
        {
            var getAvailableModelsResult = this.recommendationServiceProvider.GetModels(new GetModelsRequest());
            if (!getAvailableModelsResult.Success)
            {
                global::Sitecore.Diagnostics.Log.Error(
                    "Could not retrieve models", 
                    getAvailableModelsResult.Exception, 
                    this);
            }

            var modelList = getAvailableModelsResult.ModelList;
            return modelList;
        }

        private void TrainModel(ModelInfo model)
        {
            var result =
                this.recommendationServiceProvider.TrainItemToItemModel(
                    new TrainItemToItemModelRequest { Model = model });
            if (!result.Success)
            {
                global::Sitecore.Diagnostics.Log.Error("Could not train model", result.Exception, this);
            }
        }

        private void UploadCatalog(ModelInfo model)
        {
            var uploadCatalogResult =
                this.recommendationServiceProvider.UploadCatalog(
                    new UploadCatalogRequest
                        {
                            Model = model, 
                            CatalogDisplayName = "Some Catalog", 
                            CatalogFilePath =
                                HttpContext.Current.Server.MapPath("~/some-catalog.csv")
                        });
            if (!uploadCatalogResult.Success)
            {
                global::Sitecore.Diagnostics.Log.Error("Could not upload catalog", uploadCatalogResult.Exception, this);
            }
        }

        private void UploadUsage(ModelInfo model)
        {
            var uploadUsageBatchResult =
                this.recommendationServiceProvider.UploadUsageBatch(new UploadUsageBatchRequest
                                                                        {
                                                                            Model = model ,
                                                                         UsageDisplayName = "Some Usage", 
                            UsageFilePath = 
                                HttpContext.Current.Server.MapPath("~/some-usage-a.csv")
                                                                        });
            if (!uploadUsageBatchResult.Success)
            {
                global::Sitecore.Diagnostics.Log.Error("Could not upload usage", uploadUsageBatchResult.Exception, this);
            }
        }

        #endregion
    }
}