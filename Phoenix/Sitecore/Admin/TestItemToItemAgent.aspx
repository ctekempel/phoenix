﻿<%@ page language="C#" autoeventwireup="true" codebehind="TestItemToItemAgent.aspx.cs" inherits="Phoenix.Sitecore.Admin.TestItemToItemAgent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>
                <legend>Creating a Model</legend>
                <asp:Button runat="server" Text="Create and Train" ID="CreateAndTrainButton" OnClick="CreateAndTrainButton_OnClick"/>
            </fieldset>

            <fieldset>
                <legend>Training a Model</legend>
            </fieldset>

            <fieldset>
                <legend>Getting a Reccommendation</legend>
                <asp:Button runat="server" Text="GetRecommendations" ID="GetRecommendationsButton" OnClick="GetRecommendationsButton_OnClick"/>
                <asp:TextBox runat="server" ID="Recommendations" TextMode="MultiLine"></asp:TextBox>
            </fieldset>
        </div>
    </form>
</body>
</html>
