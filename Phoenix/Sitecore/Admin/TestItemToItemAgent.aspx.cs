﻿namespace Phoenix.Sitecore.Admin
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Phoenix.Agents;
    using Phoenix.Recommendations.ServiceProvider;

    public partial class TestItemToItemAgent : System.Web.UI.Page
    {
        #region Methods

        protected void CreateAndTrainButton_OnClick(object sender, EventArgs e)
        {
            var agent = new TrainItemToItemAgent("testModel", "/sitecore/content");
            agent.Run();
        }

        #endregion

        protected void GetRecommendationsButton_OnClick(object sender, EventArgs e)
        {
            var serviceProvider = new RecommendationServiceProvider();

            var model = serviceProvider.GetModels(new GetModelsRequest()).ModelList.Models.FirstOrDefault();
            var build = model.ActiveBuildId;
            var productIds = new[] { "5C5-00025" };

            var request = new GetItemToItemRecommendationRequest();
            request.Model = model;
            request.BuildId = build.ToString(CultureInfo.InvariantCulture);
            request.ProductIds = productIds;
            request.DesiredResults = 5;

            var result = serviceProvider.GetItemToItemRecommendation(
                request);

            this.Recommendations.Text = string.Format(
                "Success: {0} \r\n Reccommendations: {1}",
                result.Success,
                string.Join(", ", result.RecommendationSet.SelectMany(info => info.Items.Select(itemInfo => itemInfo.Id))));
        }
    }
}