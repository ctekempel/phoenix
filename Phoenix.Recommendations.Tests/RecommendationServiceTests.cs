namespace Phoenix.Recommendations.Tests
{
    using System;
    using System.IO;

    using FluentAssertions;

    using NUnit.Framework;

    using Phoenix.Recommendations.Model;
    using Phoenix.Recommendations.Model.Build;
    using Phoenix.Recommendations.Services;

    [TestFixture]
    public class RecommendationServiceTests
    {
        #region Public Methods and Operators

        [Test]
        public void ShouldGetRecommendationForItemToItemModel()
        {
            const string ProductIds = "5C5-00025";
            const int NumberOfResults = 6;

            // TODO: Reinstate
            var fbtModel = this.GetItemToItemModel();
            //var fbtModel = Tuple.Create("09c7c23f-d310-4fe6-9bff-c6cb50dff1e0", 1595178);

            var subject = new RecommendationsService(Constants.AccountKey, Constants.EndpointUri);

            var itemSets = subject.GetItemToItemRecommendations(fbtModel.Item1, fbtModel.Item2, ProductIds, NumberOfResults);

            itemSets.Should().NotBeNull();
            itemSets.RecommendedItemSetInfo.Should().HaveCount(6);

            foreach (var recommendationSet in itemSets.RecommendedItemSetInfo)
            {
                foreach (var recommendedItem in recommendationSet.Items)
                {
                    recommendedItem.Id.Should().NotBeNullOrEmpty();
                    recommendedItem.Name.Should().NotBeNullOrEmpty();
                    recommendationSet.Rating.Should().BeGreaterThan(0);
                }
            }

            // TODO: Reinstate
            this.DeleteModel(fbtModel.Item1);
        }

        [Test]
        public void ShouldGetRecommendationForUserRecommendationModel()
        {
            const int NumberOfResults = 6;
            string userId = "0003BFFDC7118D12";

            // TODO: Reinstate
            var userRecommendationModel = this.GetUserRecommendationModel();
            //var userRecommendationModel = Tuple.Create("24be4319-36b1-45ea-8882-f5bfacd7fa7e", 1595190);

            var subject = new RecommendationsService(Constants.AccountKey, Constants.EndpointUri);

            var itemSets = subject.GetUserRecommendations(
                userRecommendationModel.Item1, 
                userRecommendationModel.Item2,
                userId, 
                NumberOfResults);

            itemSets.Should().NotBeNull();
            itemSets.RecommendedItemSetInfo.Should().HaveCount(6);

            foreach (var recommendationSet in itemSets.RecommendedItemSetInfo)
            {
                foreach (var recommendedItem in recommendationSet.Items)
                {
                    recommendedItem.Id.Should().NotBeNullOrEmpty();
                    recommendedItem.Name.Should().NotBeNullOrEmpty();
                    recommendationSet.Rating.Should().BeGreaterThan(0);
                }
            }

            // TODO: Reinstate
            this.DeleteModel(userRecommendationModel.Item1);
        }

        #endregion

        #region Methods

        private static ModelInfo CreateModel()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var model = modelService.CreateModel("TestModel");
            return model;
        }

        private void DeleteModel(string modelId)
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            modelService.DeleteModel(modelId);
        }

        private Tuple<string, long> GetItemToItemModel()
        {
            var model = CreateModel();
            this.UploadCatalog(model);
            this.UploadUsage(model);
            var buildId = this.TrainItemToItemModel(model);

            return Tuple.Create(model.Id, buildId);
        }

        private Tuple<string, long> GetUserRecommendationModel()
        {
            var model = CreateModel();
            this.UploadCatalog(model);
            this.UploadUsage(model);

            var buildId = this.TrainUserRecommendationModel(model);

            return Tuple.Create(model.Id, buildId);
        }

        private long TrainItemToItemModel(ModelInfo model)
        {
            string operationLocationHeader;

            var trainingService = new TrainingService(Constants.AccountKey, Constants.EndpointUri);

            var buildId = trainingService.CreateItemToItemBuild(
                model.Id, 
                "Frequenty-Bought-Together Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"), 
                enableModelInsights: false, 
                operationLocationHeader: out operationLocationHeader);

            var buildInfo =
                trainingService.WaitForOperationCompletion<BuildInfo>(
                    TrainingService.GetOperationId(operationLocationHeader));
            return buildId;
        }

        private long TrainUserRecommendationModel(ModelInfo model)
        {
            string operationLocationHeader;

            var trainingService = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            var buildId = trainingService.CreateRecommendationsBuild(
                model.Id, 
                "Recommendation Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"), 
                enableModelInsights: false, 
                operationLocationHeader: out operationLocationHeader);

            var buildInfo =
                trainingService.WaitForOperationCompletion<BuildInfo>(
                    TrainingService.GetOperationId(operationLocationHeader));
            return buildId;
        }

        private void UploadCatalog(ModelInfo model)
        {
            var catalogTrainingService = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            var catalogFile = new FileInfo("./Resources/catalog.csv");
            catalogTrainingService.UploadCatalog(model.Id, catalogFile.FullName, catalogFile.Name);
        }

        private void UploadUsage(ModelInfo model)
        {
            var usageTrainingService = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            foreach (string usagePath in Directory.GetFiles("./Resources", "usage*.csv"))
            {
                var usageFile = new FileInfo(usagePath);
                usageTrainingService.UploadUsage(model.Id, usageFile.FullName, usageFile.Name);
            }
        }

        #endregion
    }
}