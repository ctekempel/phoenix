﻿namespace Phoenix.Recommendations.Tests
{
    public static class Constants
    {
        // Account name = Phoenix_Recommendations
        public const string AccountKey = "b2f1c1ea5bc04ad596ad131ad443a94f";

        public const string EndpointUri = "https://westus.api.cognitive.microsoft.com/recommendations/v4.0";
    }
}