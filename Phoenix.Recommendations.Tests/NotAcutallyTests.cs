﻿namespace Phoenix.Recommendations.Tests
{
    using System;

    using NUnit.Framework;

    using Phoenix.Recommendations.Services;

    [TestFixture]
    public class NotAcutallyTests
    {
        #region Public Methods and Operators

        [Test]
        [Ignore("Not actually a test")]
        public void ShouldDeleteAllModels()
        {
            var subject = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var modelsToDelete = subject.GetAllModels();

            foreach (var model in modelsToDelete.Models)
            {
                subject.DeleteModel(model.Id);
            }
        }

        [Test]
        [Ignore("Not actually a test")]
        public void ShouldDisplayAllBuildsForAllModels()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var models = modelService.GetAllModels();
            foreach (var model in models.Models)
            {
                var trainingService = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
                var builds = trainingService.GetAllBuilds(model.Id);

                foreach (var build in builds.Builds)
                {
                    Console.WriteLine(
                        "ModelId:{0} - BuildId:{1} - BuildDescription:{2}", 
                        model.Id, 
                        build.Id, 
                        build.Description);
                }
            }
        }

        [Test]
        [Ignore("Not actually a test")]
        public void ShouldDisplayAllModels()
        {
            var subject = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var modelsToDelete = subject.GetAllModels();

            foreach (var model in modelsToDelete.Models)
            {
                Console.WriteLine("{0} - {1} - {2}", model.Id, model.Name, model.Description);
            }
        }

        #endregion
    }
}