﻿namespace Phoenix.Recommendations.Tests
{
    using System.Linq;

    using FluentAssertions;

    using NUnit.Framework;

    using Phoenix.Recommendations.Model;
    using Phoenix.Recommendations.Services;

    [TestFixture]
    public class ModelServiceTests
    {
        #region Public Methods and Operators

        [Test]
        public void ShouldCreateModel()
        {
            var subject = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var description = "Some interesting model about some intereting stuff";
            ModelInfo modelInfo = subject.CreateModel("SomeModel", description);
            modelInfo.Id.Should().NotBeNull();
            modelInfo.Name.Should().Be("SomeModel");

            var cleanupService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            cleanupService.DeleteModel(modelInfo.Id);
        }

        [Test]
        public void ShouldDeleteModel()
        {
            var setupService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            ModelInfo createdModel1 = setupService.CreateModel("SomeModel-1", "blah");

            var subject = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            subject.DeleteModel(createdModel1.Id);
        }

        [Test]
        public void ShouldGetAllModels()
        {
            var setupService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            ModelInfo createdModel1 = setupService.CreateModel("SomeModel-1", "blah");
            ModelInfo createdModel2 = setupService.CreateModel("SomeModel-2", "blah");

            var subject = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var models = subject.GetAllModels();
            models.Models.Should().NotBeEmpty();
            models.Models.Count().Should().Be(2);
            models.Models.Select(info => info.Id).Should().Contain(createdModel1.Id);
            models.Models.Select(info => info.Id).Should().Contain(createdModel2.Id);

            var cleanupService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            cleanupService.DeleteModel(createdModel1.Id);
            cleanupService.DeleteModel(createdModel2.Id);
        }

        #endregion
    }
}