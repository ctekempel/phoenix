namespace Phoenix.Recommendations.Tests
{
    using System;
    using System.IO;

    using FluentAssertions;

    using NUnit.Framework;

    using Phoenix.Recommendations.Model.Build;
    using Phoenix.Recommendations.Services;

    [TestFixture]
    public class TrainingServiceTests
    {
        #region Public Methods and Operators

        [Test]
        public void ShouldUploadCatalog()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var model = modelService.CreateModel("TestModel");

            var catalogFile = new FileInfo("./Resources/catalog.csv");

            var subject = new TrainingService(Constants.AccountKey, Constants.EndpointUri);

            var result = subject.UploadCatalog(model.Id, catalogFile.FullName, catalogFile.Name);

            result.ImportedLineCount.Should().BeGreaterThan(0);
            result.ProcessedLineCount.Should().BeGreaterThan(0);
            result.ErrorLineCount.Should().Be(0);

            modelService.DeleteModel(model.Id);
        }

        [Test]
        public void ShouldUploadUsageForCatalog()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var model = modelService.CreateModel("TestModel");
            var catalogFile = new FileInfo("./Resources/catalog.csv");
            var subject = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            subject.UploadCatalog(model.Id, catalogFile.FullName, catalogFile.Name);

            var usgeFile = new FileInfo("./Resources/usageA.csv");

            var result = subject.UploadUsage(model.Id, usgeFile.FullName, usgeFile.Name);

            result.ImportedLineCount.Should().BeGreaterThan(0);
            result.ProcessedLineCount.Should().BeGreaterThan(0);

            modelService.DeleteModel(model.Id);
        }

        [Test]
        public void ShouldUploadUsageForCatalogAndTrainItemToItemModel()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var model = modelService.CreateModel("TestModel");
            var catalogFile = new FileInfo("./Resources/catalog.csv");
            var usgeFile = new FileInfo("./Resources/usageA.csv");
            var subject = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            subject.UploadCatalog(model.Id, catalogFile.FullName, catalogFile.Name);
            subject.UploadUsage(model.Id, usgeFile.FullName, usgeFile.Name);

            string operationLocationHeader;

            var buildId = subject.CreateItemToItemBuild(
                model.Id, 
                "Frequenty-Bought-Together Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"), 
                enableModelInsights: false, 
                operationLocationHeader: out operationLocationHeader);

            var buildInfo =
                subject.WaitForOperationCompletion<BuildInfo>(TrainingService.GetOperationId(operationLocationHeader));

            buildInfo.Status.Should().Be("Succeeded");

            modelService.DeleteModel(model.Id);
        }

        [Test]
        public void ShouldUploadUsageForCatalogAndTrainUserRecommendationModel()
        {
            var modelService = new ModelService(Constants.AccountKey, Constants.EndpointUri);
            var model = modelService.CreateModel("TestUserRecommendationModel");
            var catalogFile = new FileInfo("./Resources/catalog.csv");
            var usgeFile = new FileInfo("./Resources/usageA.csv");
            var subject = new TrainingService(Constants.AccountKey, Constants.EndpointUri);
            subject.UploadCatalog(model.Id, catalogFile.FullName, catalogFile.Name);
            subject.UploadUsage(model.Id, usgeFile.FullName, usgeFile.Name);

            string operationLocationHeader;

            var buildId = subject.CreateRecommendationsBuild(
                model.Id, 
                "Recommendation Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"), 
                enableModelInsights: false, 
                operationLocationHeader: out operationLocationHeader);

            var buildInfo =
                subject.WaitForOperationCompletion<BuildInfo>(TrainingService.GetOperationId(operationLocationHeader));

            buildInfo.Status.Should().Be("Succeeded");

            modelService.DeleteModel(model.Id);
        }

        #endregion
    }
}