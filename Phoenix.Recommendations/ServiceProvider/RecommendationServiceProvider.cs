﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;

    public class RecommendationServiceProvider : PhoenixServiceProvider
    {
        public virtual CreateModelResult CreateModel(CreateModelRequest request)
        {
            return this.RunPipeline<CreateModelRequest, CreateModelResult>("phoenix.recommendations.createModel", request);
        }

        public virtual GetModelsResult GetModels(GetModelsRequest request)
        {
            return this.RunPipeline<GetModelsRequest, GetModelsResult>("phoenix.recommendations.getModels", request);
        }

        public virtual GetModelResult GetModel(GetModelRequest request)
        {
            return this.RunPipeline<GetModelRequest, GetModelResult>("phoenix.recommendations.GetModel", request);
        }

        public virtual UploadCatalogResult UploadCatalog(UploadCatalogRequest request)
        {
            return this.RunPipeline<UploadCatalogRequest, UploadCatalogResult>("phoenix.recommendations.uploadCatalog", request);
        }

        public virtual UploadUsageBatchResult UploadUsageBatch(UploadUsageBatchRequest request)
        {
            return this.RunPipeline<UploadUsageBatchRequest, UploadUsageBatchResult>("phoenix.recommendations.uploadUsageBatch", request);
        }

        public virtual UploadUsageSingleResult UploadUsageSingle(UpoloadUsageSingleRequest request)
        {
            return this.RunPipeline<UpoloadUsageSingleRequest, UploadUsageSingleResult>("phoenix.recommendations.uploadUsageSingle", request);
        }

        public virtual TrainItemToItemModelResult TrainItemToItemModel(TrainItemToItemModelRequest request)
        {
            return this.RunPipeline<TrainItemToItemModelRequest, TrainItemToItemModelResult>("phoenix.recommendations.ItemToItem.TrainModel", request);
        }

        public virtual GetItemToItemRecommendationResult GetItemToItemRecommendation(GetItemToItemRecommendationRequest request)
        {
            return this.RunPipeline<GetItemToItemRecommendationRequest, GetItemToItemRecommendationResult>("phoenix.recommendations.ItemToItem.GetRecommendation", request);
        }

        public virtual TrainUserRecommendationsModelResult TrainUserRecommendationsModel(TrainUserRecommendationsModelRequest request)
        {
            return this.RunPipeline<TrainUserRecommendationsModelRequest, TrainUserRecommendationsModelResult>("phoenix.recommendations.ItemToItem.TrainModel", request);
        }

        public virtual GetUserRecommendationsRecommendationResult GetUserRecommendationsRecommendation(GetUserRecommendationsRecommendationRequest request)
        {
            return this.RunPipeline<GetUserRecommendationsRecommendationRequest, GetUserRecommendationsRecommendationResult>("phoenix.recommendations.ItemToItem.GetRecommendation", request);
        }
    }
}