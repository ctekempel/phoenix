﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class TrainItemToItemModelRequest : PhoenixServiceProviderRequest
    {
        public ModelInfo Model { get; set; }
    }
}