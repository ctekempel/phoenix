﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using System.Collections.Generic;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model.Recommendations;

    public class GetItemToItemRecommendationResult : PhoenixServiceProviderResult
    {
        public IEnumerable<RecommendedItemSetInfo> RecommendationSet { get; set; }
    }
}