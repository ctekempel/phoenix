﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class UploadUsageBatchRequest : PhoenixServiceProviderRequest
    {
        public ModelInfo Model { get; set; }

        public string UsageFilePath { get; set; }

        public string UsageDisplayName { get; set; }
    }
}