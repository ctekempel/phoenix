﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class UploadCatalogRequest : PhoenixServiceProviderRequest
    {
        public ModelInfo Model { get; set; }

        public string CatalogFilePath { get; set; }

        public string CatalogDisplayName { get; set; }
    }
}