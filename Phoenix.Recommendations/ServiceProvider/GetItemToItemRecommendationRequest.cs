﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using System.Collections.Generic;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class GetItemToItemRecommendationRequest : PhoenixServiceProviderRequest
    {
        #region Public Properties

        public string BuildId { get; set; }

        public ModelInfo Model { get; set; }

        public IEnumerable<string> ProductIds { get; set; }

        public int? DesiredResults { get; set; }

        #endregion
    }
}