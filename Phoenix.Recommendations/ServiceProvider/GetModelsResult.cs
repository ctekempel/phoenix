﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class GetModelsResult : PhoenixServiceProviderResult
    {
        public ModelInfoList ModelList { get; set; }
    }
}