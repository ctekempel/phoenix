namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model;

    public class CreateModelResult : PhoenixServiceProviderResult
    {
        public ModelInfo Model { get; set; }
    }
}