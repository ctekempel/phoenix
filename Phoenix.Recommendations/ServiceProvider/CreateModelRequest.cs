﻿namespace Phoenix.Recommendations.ServiceProvider
{
    using Phoenix.Foundation.ServiceProvider;

    public class CreateModelRequest : PhoenixServiceProviderRequest
    {
        public string ModelName { get; set; }

        public string ModelDescription { get; set; }
    }
}