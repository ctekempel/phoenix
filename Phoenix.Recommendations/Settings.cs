namespace Phoenix.Recommendations
{
    public static class Settings
    {
        private const string RecommendationsApipath = "Phoenix.Recommendations.ApiPath";

        public static string RecommendationsServiceEndpoint
        {
            get
            {
                var cognitiveServicesEndpoint = Foundation.Settings.AzureCognitiveServicesEndpoint;
                var apiPath = Sitecore.Configuration.Settings.GetSetting(RecommendationsApipath);

                string endpointUri = cognitiveServicesEndpoint + apiPath;

                return endpointUri;
            }
        }
    }
}