namespace Phoenix.Recommendations.Model.Splitting
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class RandomSplitterParameters
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("randomSeed")]
        public int? RandomSeed { get; set; }

        [DataMember]
        [JsonProperty("testPercent")]
        public int? TestPercent { get; set; }

        #endregion
    }
}