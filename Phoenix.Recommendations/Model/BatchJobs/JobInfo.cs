﻿namespace Phoenix.Recommendations.Model.BatchJobs
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class JobInfo
    {
        #region Public Properties

        /// <summary>
        /// Api Name
        /// The ApiName is internally an enum (see SupportedApis in BatchScoringManager)
        /// The valid values should be: ItemRecommend, UserRecommend, ItemFbtRecommend
        /// </summary>
        [JsonProperty("apiName")]
        public string ApiName { get; set; }

        /// <summary>
        /// The build Id
        /// BuildId is the build id which batch scoring is requested to
        /// It is optional. If it is not provided, the active build id will be used
        /// </summary>
        [JsonProperty("buildId")]
        public long BuildId { get; set; }

        /// <summary>
        /// Include Metadata
        /// it indicates whether the result should include metadata or not
        /// It is optional. The default value is false
        /// </summary>
        [JsonProperty("includeMetadata")]
        public bool IncludeMetadata { get; set; }

        /// <summary>
        /// The minimum score. Currently only supported for FbtBuilds
        /// It indicates the minimal score to return
        /// It is optional. The default value is 0.1
        /// </summary>
        [JsonProperty("minimalScore")]
        public double MinimalScore { get; set; }

        /// <summary>
        /// The Model Id
        /// ModelId is the model id which batch scoring is requested to
        /// </summary>
        [JsonProperty("modelId")]
        public string ModelId { get; set; }

        /// <summary>
        /// Number of recommendations
        /// It indicates the number of results (recommended items) each request should return
        /// It is optional. The default value is 10
        /// </summary>
        [JsonProperty("numberOfResults")]
        public int NumberOfResults { get; set; }

        #endregion
    }
}