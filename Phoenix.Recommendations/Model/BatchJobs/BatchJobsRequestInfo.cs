namespace Phoenix.Recommendations.Model.BatchJobs
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    using Phoenix.Recommendations.Model.Storage;

    [DataContract]
    public class BatchJobsRequestInfo
    {
        #region Public Properties

        /// <summary>
        /// The error storage blob info
        /// </summary>
        [JsonProperty("error")]
        public StorageBlobInfo Error { get; set; }

        /// <summary>
        /// The input storage blob info
        /// </summary>
        [JsonProperty("input")]
        public StorageBlobInfo Input { get; set; }

        /// <summary>
        /// The job info
        /// </summary>
        [JsonProperty("job")]
        public JobInfo Job { get; set; }

        /// <summary>
        /// The output storage blob info
        /// </summary>
        [JsonProperty("output")]
        public StorageBlobInfo Output { get; set; }

        #endregion
    }
}