﻿namespace Phoenix.Recommendations.Model.BatchJobs
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class BatchJobsResponse
    {
        #region Public Properties

        /// <summary>
        /// Unique batch job identifier 
        /// </summary>
        [DataMember]
        [JsonProperty("batchId")]
        public string BatchId { get; set; }

        #endregion
    }
}