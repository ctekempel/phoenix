﻿namespace Phoenix.Recommendations.Model
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class ModelRequestInfo
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("description")]
        public string Description { get; set; }

        [DataMember]
        [JsonProperty("modelName")]
        public string ModelName { get; set; }

        #endregion
    }
}