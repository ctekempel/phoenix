namespace Phoenix.Recommendations.Model
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class ModelInfo
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("activeBuildId")]
        public long ActiveBuildId { get; set; }

        [DataMember]
        [JsonProperty("createdDateTime")]
        public string CreatedDateTime { get; set; }

        [DataMember]
        [JsonProperty("description")]
        public string Description { get; set; }

        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember]
        [JsonProperty("name")]
        public string Name { get; set; }

        #endregion
    }
}