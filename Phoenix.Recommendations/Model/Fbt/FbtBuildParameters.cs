﻿namespace Phoenix.Recommendations.Model.Fbt
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    using Phoenix.Recommendations.Model.Splitting;

    [DataContract]
    public class FbtBuildParameters
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("enableModelingInsights")]
        public bool? EnableModelingInsights { get; set; }

        [DataMember]
        [JsonProperty("maxItemSetSize")]
        public int? MaxItemSetSize { get; set; }

        [DataMember]
        [JsonProperty("minimalScore")]
        public double? MinimalScore { get; set; }

        [DataMember]
        [JsonProperty("randomSplitterParameters")]
        public RandomSplitterParameters RandomSplitterParameters { get; set; }

        [DataMember]
        [JsonProperty("similarityFunction")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FbtSimilarityFunction SimilarityFunction { get; set; }

        [DataMember]
        [JsonProperty("splitterStrategy")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SplitterStrategy SplitterStrategy { get; set; }

        [DataMember]
        [JsonProperty("supportThreshold")]
        public int? SupportThreshold { get; set; }

        #endregion
    }
}