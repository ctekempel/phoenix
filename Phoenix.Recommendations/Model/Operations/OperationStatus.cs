namespace Phoenix.Recommendations.Model.Operations
{
    public enum OperationStatus
    {
        NotStarted = 0, // build is in queue waiting for execution

        Running = 1, // build is in progress

        Cancelling = 2, // build is in the process of cancellation

        Cancelled = 3, // build was cancelled

        Succeeded = 4, // build ended with success

        Failed = 5, // build ended with error
    }
}