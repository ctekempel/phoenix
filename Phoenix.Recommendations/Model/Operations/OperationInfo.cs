namespace Phoenix.Recommendations.Model.Operations
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    public class OperationInfo<T>
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("createdDateTime")]
        public string CreatedDateTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("lastActionDateTime", NullValueHandling = NullValueHandling.Ignore)]
        public string LastActionDateTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("percentComplete", NullValueHandling = NullValueHandling.Ignore)]
        public int PercentComplete { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("resourceLocation", NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceLocation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public T Result { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public string Status { get; set; }

        [DataMember]
        [JsonProperty("type")]
        public string Type { get; set; }

        #endregion
    }
}