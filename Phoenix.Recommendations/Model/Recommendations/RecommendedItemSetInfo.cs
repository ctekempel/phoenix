﻿namespace Phoenix.Recommendations.Model.Recommendations
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    /// <summary>
    /// Holds a recommendation result, which is a set of recommended items with reasoning and rating/score.
    /// </summary>
    [DataContract]
    public class RecommendedItemSetInfo
    {
        #region Constructors and Destructors

        public RecommendedItemSetInfo()
        {
            this.Items = new List<RecommendedItemInfo>();
        }

        #endregion

        #region Public Properties

        [DataMember]
        [JsonProperty("items")]
        public IEnumerable<RecommendedItemInfo> Items { get; set; }

        [DataMember]
        [JsonProperty("rating")]
        public double Rating { get; set; }

        [DataMember]
        [JsonProperty("reasoning")]
        public IEnumerable<string> Reasoning { get; set; }

        #endregion
    }
}