﻿namespace Phoenix.Recommendations.Model.Recommendations
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class RecommendedItemSetInfoList
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("recommendedItems")]
        public IEnumerable<RecommendedItemSetInfo> RecommendedItemSetInfo { get; set; }

        #endregion
    }
}