﻿namespace Phoenix.Recommendations.Model.Recommendations
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class RecommendedItemInfo
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember]
        [JsonProperty("metadata")]
        public string Metadata { get; set; }

        [DataMember]
        [JsonProperty("name")]
        public string Name { get; set; }

        #endregion
    }
}