﻿namespace Phoenix.Recommendations.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class ModelInfoList
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("models")]
        public IEnumerable<ModelInfo> Models { get; set; }

        #endregion
    }
}