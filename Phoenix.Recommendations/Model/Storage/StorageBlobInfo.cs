﻿namespace Phoenix.Recommendations.Model.Storage
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class StorageBlobInfo
    {
        #region Public Properties

        /// <summary>
        /// Authentication Type
        /// value "PublicOrSas"
        /// </summary>
        [JsonProperty("authenticationType")]
        public string AuthenticationType { get; set; }

        /// <summary>
        /// Base Location
        /// ex: "https://{storage name}.blob.core.windows.net/"
        /// </summary>
        [JsonProperty("baseLocation")]
        public string BaseLocation { get; set; }

        /// <summary>
        /// The relative location, including the container name
        /// </summary>
        [JsonProperty("relativeLocation")]
        public string RelativeLocation { get; set; }

        /// <summary>
        /// The sasToken to access the file
        /// </summary>
        [JsonProperty("sasBlobToken")]
        public string SasBlobToken { get; set; }

        #endregion
    }
}