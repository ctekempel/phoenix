﻿namespace Phoenix.Recommendations.Model.Import
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class ImportErrorStats
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [DataMember]
        [JsonProperty("errorCodeCount")]
        public int ErrorCodeCount { get; set; }

        #endregion
    }
}