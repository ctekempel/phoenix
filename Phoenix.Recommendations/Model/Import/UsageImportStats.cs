﻿namespace Phoenix.Recommendations.Model.Import
{
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class UsageImportStats : CatalogImportStats
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("fileId")]
        public string FileId { get; set; }

        #endregion
    }
}