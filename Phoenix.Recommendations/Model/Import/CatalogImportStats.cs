namespace Phoenix.Recommendations.Model.Import
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract]
    public class CatalogImportStats
    {
        #region Public Properties

        [DataMember]
        [JsonProperty("errorLineCount")]
        public int ErrorLineCount { get; set; }

        [DataMember]
        [JsonProperty("errorSummary")]
        public IEnumerable<ImportErrorStats> ErrorSummary { get; set; }

        [DataMember]
        [JsonProperty("importedLineCount")]
        public int ImportedLineCount { get; set; }

        [DataMember]
        [JsonProperty("processedLineCount")]
        public int ProcessedLineCount { get; set; }

        #endregion
    }
}