namespace Phoenix.Recommendations.Model.Import
{
    /// <summary>
    /// Utility class holding the result of import operation
    /// </summary>
    public class ImportReport
    {
        #region Public Properties

        public int ErrorCount { get; set; }

        public string Info { get; set; }

        public int LineCount { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format(
                "successfully imported {0}/{1} lines for {2}", 
                this.LineCount - this.ErrorCount, 
                this.LineCount, 
                this.Info);
        }

        #endregion
    }
}