﻿namespace Phoenix.Recommendations.Services
{
    using System;

    using Newtonsoft.Json;

    using Phoenix.Recommendations.Model.Recommendations;

    public class RecommendationsService : AzureRecommendationsServiceBase
    {
        #region Constructors and Destructors

        public RecommendationsService(string accountKey, string baseUri)
            : base(accountKey, baseUri)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Get Item to Item (I2I) Recommendations or Frequently-Bought-Together (FBT) recommendations
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <param name="buildId">The build identifier.</param>
        /// <param name="itemIds"></param>
        /// <param name="numberOfResults"></param>
        /// <returns>
        /// The recommendation sets. Note that I2I builds will only return one item per set.
        /// FBT builds will return more than one item per set.
        /// </returns>
        public RecommendedItemSetInfoList GetItemToItemRecommendations(
            string modelId, 
            long buildId, 
            string itemIds, 
            int numberOfResults)
        {
            string uri = this.BaseUri + "/models/" + modelId + "/recommend/item?itemIds=" + itemIds
                         + "&numberOfResults=" + numberOfResults + "&minimalScore=0";
            var response = this.HttpClient.GetAsync(uri).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to get recommendations for modelId {1}, buildId {2}, Reason: {3}", 
                        response.StatusCode, 
                        modelId, 
                        buildId, 
                        ExtractErrorInfo(response)));
            }

            var jsonString = response.Content.ReadAsStringAsync().Result;
            var recommendedItemSetInfoList = JsonConvert.DeserializeObject<RecommendedItemSetInfoList>(jsonString);
            return recommendedItemSetInfoList;
        }

        /// <summary>
        /// Use historical transaction data to provide personalized recommendations for a user.
        /// The user history is extracted from the usage files used to train the model.
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <param name="buildId">The build identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="numberOfResults">Desired number of recommendation results.</param>
        /// <returns>The recommendations for the user.</returns>
        public RecommendedItemSetInfoList GetUserRecommendations(
            string modelId,
            long buildId,
            string userId,
            int numberOfResults)
        {
            string uri = this.BaseUri + "/models/" + modelId + "/recommend/user?userId=" + userId + "&numberOfResults="
                         + numberOfResults;
            var response = this.HttpClient.GetAsync(uri).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to get user recommendations for modelId {1}, buildId {2}, Reason: {3}",
                        response.StatusCode,
                        modelId,
                        buildId,
                        ExtractErrorInfo(response)));
            }

            var jsonString = response.Content.ReadAsStringAsync().Result;
            var recommendedItemSetInfoList = JsonConvert.DeserializeObject<RecommendedItemSetInfoList>(jsonString);
            return recommendedItemSetInfoList;
        }

        #endregion
    }
}