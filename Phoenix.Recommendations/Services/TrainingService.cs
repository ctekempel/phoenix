namespace Phoenix.Recommendations.Services
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;

    using Newtonsoft.Json;

    using Phoenix.Recommendations.Model.Build;
    using Phoenix.Recommendations.Model.Fbt;
    using Phoenix.Recommendations.Model.Import;
    using Phoenix.Recommendations.Model.Operations;
    using Phoenix.Recommendations.Model.Splitting;

    public class TrainingService : AzureRecommendationsServiceBase
    {
        #region Constructors and Destructors

        public TrainingService(string accountKey, string baseUri)
            : base(accountKey, baseUri)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Extract the operation id from the operation header
        /// </summary>
        /// <param name="operationLocationHeader"></param>
        /// <returns></returns>
        public static string GetOperationId(string operationLocationHeader)
        {
            int index = operationLocationHeader.LastIndexOf('/');
            var operationId = operationLocationHeader.Substring(index + 1);
            return operationId;
        }

        /// <summary>
        /// Submit a model build, with passed build parameters.
        /// </summary>
        /// <param name="modelId">Unique identifier of the model</param>
        /// <param name="buildRequestInfo">Build parameters</param>
        /// <param name="operationLocationHeader">Build operation location</param>
        /// <returns>The build id.</returns>
        public long BuildModel(string modelId, BuildRequestInfo buildRequestInfo, out string operationLocationHeader)
        {
            string uri = this.BaseUri + "/models/" + modelId + "/builds";
            var response = this.HttpClient.PostAsJsonAsync(uri, buildRequestInfo).Result;
            var jsonString = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to start build for model {1}, \n reason {2}", 
                        response.StatusCode, 
                        modelId, 
                        ExtractErrorInfo(response)));
            }

            operationLocationHeader = response.Headers.GetValues("Operation-Location").FirstOrDefault();
            var buildModelResponse = JsonConvert.DeserializeObject<BuildModelResponse>(jsonString);
            return buildModelResponse.BuildId;
        }

        /// <summary>
        /// Trigger a recommendation build for the given model.
        /// Note: unless configured otherwise the u2i (user to item/user based) recommendations are enabled too.
        /// </summary>
        /// <param name="modelId">the model id</param>
        /// <param name="buildDescription">a description for the build</param>
        /// <param name="enableModelInsights">????</param>
        /// <param name="operationLocationHeader">operation location header, can be used to cancel the build operation and to get status.</param>
        /// <returns>Unique indentifier of the build initiated.</returns>
        public long CreateItemToItemBuild(
            string modelId, 
            string buildDescription, 
            bool enableModelInsights, 
            out string operationLocationHeader)
        {
            // only used if splitter strategy is set to RandomSplitter
            var randomSplitterParameters = new RandomSplitterParameters { RandomSeed = 0, TestPercent = 10 };

            var parameters = new FbtBuildParameters
                                 {
                                     MinimalScore = 0, 
                                     SimilarityFunction = FbtSimilarityFunction.Lift, 
                                     SupportThreshold = 3, 
                                     MaxItemSetSize = 2, 
                                     EnableModelingInsights = enableModelInsights, 
                                     SplitterStrategy = SplitterStrategy.LastEventSplitter, 
                                     RandomSplitterParameters = randomSplitterParameters, 
                                 };

            var requestInfo = new BuildRequestInfo
                                  {
                                      BuildType = BuildType.Fbt, 
                                      BuildParameters = new BuildParameters { Fbt = parameters, }, 
                                      Description = buildDescription
                                  };

            return this.BuildModel(modelId, requestInfo, out operationLocationHeader);
        }

        /// <summary>
        /// Trigger a recommendation build for the given model.
        /// Note: unless configured otherwise the u2i (user to item/user based) recommendations are enabled too.
        /// </summary>
        /// <param name="modelId">the model id</param>
        /// <param name="buildDescription">a description for the build</param>
        /// <param name="enableModelInsights"> true to enable modeling insights, selects "LastEventSplitter" as the splitting strategy by default. </param>
        /// <param name="operationLocationHeader">operation location header, can be used to cancel the build operation and to get status.</param>
        /// <returns>Unique indentifier of the build initiated.</returns>
        public long CreateRecommendationsBuild(
            string modelId, 
            string buildDescription, 
            bool enableModelInsights, 
            out string operationLocationHeader)
        {
            // only used if splitter strategy is set to RandomSplitter
            var randomSplitterParameters = new RandomSplitterParameters { RandomSeed = 0, TestPercent = 10 };

            var parameters = new RecommendationBuildParameters
                                 {
                                     NumberOfModelIterations = 10, 
                                     NumberOfModelDimensions = 20, 
                                     ItemCutOffLowerBound = 1, 
                                     EnableModelingInsights = enableModelInsights, 
                                     SplitterStrategy = SplitterStrategy.LastEventSplitter, 
                                     RandomSplitterParameters = randomSplitterParameters, 
                                     EnableU2I = true, 
                                     UseFeaturesInModel = false, 
                                     AllowColdItemPlacement = false, 
                                 };

            var requestInfo = new BuildRequestInfo
                                  {
                                      BuildType = BuildType.Recommendation, 
                                      BuildParameters =
                                          new BuildParameters { Recommendation = parameters }, 
                                      Description = buildDescription
                                  };

            return this.BuildModel(modelId, requestInfo, out operationLocationHeader);
        }

        public BuildInfoList GetAllBuilds(string modelId)
        {
            string uri = this.BaseUri + "/models/" + modelId + "/builds";
            var response = this.HttpClient.GetAsync(uri).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to delete modelId {1}, \n reason {2}", 
                        response.StatusCode, 
                        modelId, 
                        ExtractErrorInfo(response)));
            }

            var jsonString = ExtractReponse(response);
            var buildInfoList = JsonConvert.DeserializeObject<BuildInfoList>(jsonString);
            return buildInfoList;
        }

        /// <summary>
        /// Upload catalog items to a model.
        /// </summary>
        /// <param name="modelId">Unique identifier of the model</param>
        /// <param name="catalogFilePath">Catalog file path</param>
        /// <param name="catalogDisplayName">Name for the catalog</param>
        /// <returns>Statistics about the catalog import operation.</returns>
        public CatalogImportStats UploadCatalog(string modelId, string catalogFilePath, string catalogDisplayName)
        {
            Console.WriteLine("Uploading " + catalogDisplayName + " ...");
            string uri = this.BaseUri + "/models/" + modelId + "/catalog?catalogDisplayName=" + catalogDisplayName;
            using (var filestream = new FileStream(catalogFilePath, FileMode.Open, FileAccess.Read))
            {
                var response = this.HttpClient.PostAsync(uri, new StreamContent(filestream)).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(
                        string.Format(
                            "Error {0}: Failed to import catalog items {1}, for model {2} \n reason {3}", 
                            response.StatusCode, 
                            catalogFilePath, 
                            modelId, 
                            ExtractErrorInfo(response)));
                }

                var jsonString = ExtractReponse(response);
                var catalogImportStats = JsonConvert.DeserializeObject<CatalogImportStats>(jsonString);
                return catalogImportStats;
            }
        }

        /// <summary>
        /// Upload usage data to a model.
        /// Usage files must be less than 200 MB.
        /// If you need to upload more than 200 MB, you may call this function multiple times.
        /// </summary>
        /// <param name="modelId">Unique identifier of the model</param>
        /// <param name="usageFilePath">Usage file path</param>
        /// <param name="usageDisplayName">Name for the usage data being uploaded</param>
        /// <returns>Statistics about the usage upload operation.</returns>
        public UsageImportStats UploadUsage(string modelId, string usageFilePath, string usageDisplayName)
        {
            Console.WriteLine("Uploading " + usageDisplayName + " ...");

            string uri = this.BaseUri + "/models/" + modelId + "/usage?usageDisplayName=" + usageDisplayName;

            using (var filestream = new FileStream(usageFilePath, FileMode.Open, FileAccess.Read))
            {
                var response = this.HttpClient.PostAsync(uri, new StreamContent(filestream)).Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(
                        string.Format(
                            "Error {0}: Failed to import usage data {1}, for model {2} \n reason {3}", 
                            response.StatusCode, 
                            usageFilePath, 
                            modelId, 
                            ExtractErrorInfo(response)));
                }

                var jsonString = ExtractReponse(response);
                var usageImportStats = JsonConvert.DeserializeObject<UsageImportStats>(jsonString);
                return usageImportStats;
            }
        }

        /// <summary>
        /// Monitor operation status and wait for completion.
        /// </summary>
        /// <param name="operationId">The operation id</param>
        /// <returns>Build status</returns>
        public OperationInfo<T> WaitForOperationCompletion<T>(string operationId)
        {
            OperationInfo<T> operationInfo;

            string uri = this.BaseUri + "/operations";

            while (true)
            {
                var response = this.HttpClient.GetAsync(uri + "/" + operationId).Result;
                var jsonString = response.Content.ReadAsStringAsync().Result;
                operationInfo = JsonConvert.DeserializeObject<OperationInfo<T>>(jsonString);

                // Operation status {NotStarted, Running, Cancelling, Cancelled, Succeded, Failed}
                Console.WriteLine(" Operation Status: {0}. \t Will check again in 10 seconds.", operationInfo.Status);

                if (OperationStatus.Succeeded.ToString().Equals(operationInfo.Status)
                    || OperationStatus.Failed.ToString().Equals(operationInfo.Status)
                    || OperationStatus.Cancelled.ToString().Equals(operationInfo.Status))
                {
                    break;
                }

                Thread.Sleep(TimeSpan.FromSeconds(10));
            }

            return operationInfo;
        }

        #endregion
    }
}