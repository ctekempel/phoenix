﻿namespace Phoenix.Recommendations.Services
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Formatting;

    using Newtonsoft.Json;

    using Phoenix.Recommendations.Model;
    using Phoenix.Recommendations.Model.BatchJobs;
    using Phoenix.Recommendations.Model.Build;

    /// <summary>
    /// A wrapper class to invoke Recommendations REST APIs
    /// </summary>
    public class AzureRecommendationsService : AzureRecommendationsServiceBase
    {
        #region Constructors and Destructors

        public AzureRecommendationsService(string accountKey, string baseUri)
            : base(accountKey, baseUri)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Creates a new model.
        /// </summary>
        /// <param name="modelName">Name for the model</param>
        /// <param name="description">Description for the model</param>
        /// <returns>Model Information.</returns>
        public ModelInfo CreateModel(string modelName, string description = null)
        {
            var uri = this.BaseUri + "/models/";
            var modelRequestInfo = new ModelRequestInfo { ModelName = modelName, Description = description };
            var response = this.HttpClient.PostAsJsonAsync(uri, modelRequestInfo).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to create model {1}, \n reason {2}", 
                        response.StatusCode, 
                        modelName, 
                        ExtractErrorInfo(response)));
            }

            var jsonString = ExtractReponse(response);
            var modelInfo = JsonConvert.DeserializeObject<ModelInfo>(jsonString);
            return modelInfo;
        }

        /// <summary>
        /// Delete a certain build of a model.
        /// </summary>
        /// <param name="modelId">Unique identifier of the model</param>
        /// <param name="buildId">Unique identifier of the build</param>
        public void DeleteBuild(string modelId, long buildId)
        {
            string uri = this.BaseUri + "/models/" + modelId + "/builds/" + buildId;
            var response = this.HttpClient.DeleteAsync(uri).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to delete buildId {1} for modelId {2}, \n reason {3}", 
                        response.StatusCode, 
                        buildId, 
                        modelId, 
                        ExtractErrorInfo(response)));
            }
        }

        /// <summary>
        /// Set an active build for the model.
        /// </summary>
        /// <param name="modelId">Unique idenfier of the model</param>
        /// <param name="updateActiveBuildInfo"></param>
        public void SetActiveBuild(string modelId, UpdateActiveBuildInfo updateActiveBuildInfo)
        {
            string uri = this.BaseUri + "/models/" + modelId;
            var content = new ObjectContent<UpdateActiveBuildInfo>(updateActiveBuildInfo, new JsonMediaTypeFormatter());
            var request = new HttpRequestMessage(new HttpMethod("PATCH"), uri) { Content = content };
            var response = this.HttpClient.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Error HTTP Status Code");
            }
        }

        /// <summary>
        /// Update model information
        /// </summary>
        /// <param name="modelId">the id of the model</param>
        /// <param name="activeBuildId">the id of the build to be active (optional)</param>
        public void SetActiveBuild(string modelId, long activeBuildId)
        {
            var info = new UpdateActiveBuildInfo { ActiveBuildId = activeBuildId };

            this.SetActiveBuild(modelId, info);
        }

        /// <summary>
        /// Request the batch job
        /// </summary>
        /// <param name="batchJobsRequestInfo">The batch job request information</param>
        /// <param name="operationLocationHeader">Batch operation location</param>
        /// <returns></returns>
        public string StartBatchJob(BatchJobsRequestInfo batchJobsRequestInfo, out string operationLocationHeader)
        {
            string uri = this.BaseUri + "/batchjobs";
            var response = this.HttpClient.PostAsJsonAsync(uri, batchJobsRequestInfo).Result;
            var jsonString = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to submit the batch job for model {1}, Reason: {2}", 
                        response.StatusCode, 
                        batchJobsRequestInfo.Job.ModelId, 
                        ExtractErrorInfo(response)));
            }

            operationLocationHeader = response.Headers.GetValues("Operation-Location").FirstOrDefault();
            var batchJobResponse = JsonConvert.DeserializeObject<BatchJobsResponse>(jsonString);
            return batchJobResponse.BatchId;
        }

        #endregion
    }
}