﻿namespace Phoenix.Recommendations.Services
{
    using System;
    using System.Net.Http;

    using Newtonsoft.Json;

    using Phoenix.Recommendations.Model;

    public class ModelService : AzureRecommendationsServiceBase
    {

        #region Public Methods and Operators

        public ModelService(string accountKey, string baseUri)
            : base(accountKey, baseUri)
        {
        }

        /// <summary>
        /// Creates a new model.
        /// </summary>
        /// <param name="modelName">Name for the model</param>
        /// <param name="description">Description for the model</param>
        /// <returns>Model Information.</returns>
        public ModelInfo CreateModel(string modelName, string description = null)
        {
            var uri = this.BaseUri + "/models/";
            var modelRequestInfo = new ModelRequestInfo { ModelName = modelName, Description = description };
            var response = this.HttpClient.PostAsJsonAsync(uri, modelRequestInfo).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to create model {1}, \n reason {2}", 
                        response.StatusCode, 
                        modelName, 
                        ExtractErrorInfo(response)));
            }

            var jsonString = ExtractReponse(response);
            var modelInfo = JsonConvert.DeserializeObject<ModelInfo>(jsonString);
            return modelInfo;
        }

        /// <summary>
        /// Delete a model, also the associated catalog/usage data and any builds.
        /// </summary>
        /// <param name="modelId">Unique identifier of the model</param>
        public void DeleteModel(string modelId)
        {
            string uri = this.BaseUri + "/models/" + modelId;
            var response = this.HttpClient.DeleteAsync(uri).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to delete modelId {1}, \n reason {2}", 
                        response.StatusCode, 
                        modelId, 
                        ExtractErrorInfo(response)));
            }
        }

        public ModelInfoList GetAllModels()
        {
            var uri = this.BaseUri + "/models";
            var response = this.HttpClient.GetAsync(uri).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(
                    string.Format(
                        "Error {0}: Failed to get models, \n reason {1}", 
                        response.StatusCode, 
                        ExtractErrorInfo(response)));
            }

            var jsonString = ExtractReponse(response);
            var modelInfo = JsonConvert.DeserializeObject<ModelInfoList>(jsonString);
            return modelInfo;
        }

        #endregion
    }
}