namespace Phoenix.Recommendations.Services
{
    using System;
    using System.Net.Http;

    public class AzureRecommendationsServiceBase
    {
        #region Fields

        protected readonly string BaseUri;

        protected readonly HttpClient HttpClient;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Constructor that initializes the Http Client.
        /// </summary>
        /// <param name="accountKey">The account key</param>
        /// <param name="baseUri">The uri</param>
        protected AzureRecommendationsServiceBase(string accountKey, string baseUri)
        {
            this.BaseUri = baseUri;

            this.HttpClient = new HttpClient
                                  {
                                      BaseAddress = new Uri(this.BaseUri),
                                      Timeout = TimeSpan.FromMinutes(15),
                                      DefaultRequestHeaders = { { "Ocp-Apim-Subscription-Key", accountKey } }
                                  };
        }

        #endregion

        #region Methods

        /// <summary>
        /// Extract error message from the httpResponse, (reason phrase + body)
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected static string ExtractErrorInfo(HttpResponseMessage response)
        {
            string detailedReason = null;
            if (response.Content != null)
            {
                detailedReason = response.Content.ReadAsStringAsync().Result;
            }

            var errorMsg = detailedReason == null
                               ? response.ReasonPhrase
                               : response.ReasonPhrase + "->" + detailedReason;
            return errorMsg;
        }

        /// <summary>
        /// Extract error information from HTTP response message.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected static string ExtractReponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }

            string detailedReason = null;
            if (response.Content != null)
            {
                detailedReason = response.Content.ReadAsStringAsync().Result;
            }

            var errorMsg = detailedReason == null
                               ? response.ReasonPhrase
                               : response.ReasonPhrase + "->" + detailedReason;

            string error = string.Format(
                "Status code: {0}\nDetail information: {1}",
                (int)response.StatusCode,
                errorMsg);
            throw new Exception("Response: " + error);
        }

        #endregion
    }
}