﻿namespace Phoenix.Recommendations.Pipelines.Catalog
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class UploadCatalog
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (UploadCatalogRequest)args.Request;
            Assert.ArgumentNotNull(request.Model, "args.request.Model");

            var result = (UploadCatalogResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var trainingService = new TrainingService(accountKey, recommendationsServiceEndpoint);

                var stats = trainingService.UploadCatalog(request.Model.Id, request.CatalogFilePath, request.CatalogDisplayName);

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}