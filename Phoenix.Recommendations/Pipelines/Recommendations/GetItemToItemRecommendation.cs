﻿namespace Phoenix.Recommendations.Pipelines.Recommendations
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class GetItemToItemRecommendation
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (GetItemToItemRecommendationRequest)args.Request;
            Assert.ArgumentNotNull(request.Model, "args.request.Model");
            Assert.ArgumentNotNull(request.BuildId, "args.request.BuildId");
            Assert.ArgumentNotNull(request.ProductIds, "args.request.ProductIds");
            Assert.ArgumentNotNull(request.DesiredResults, "args.request.DesiredResults");

            var result = (GetItemToItemRecommendationResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var subject = new RecommendationsService(accountKey, recommendationsServiceEndpoint);

                var itemSets = subject.GetItemToItemRecommendations(
                    request.Model.Id, 
                    long.Parse(request.BuildId), 
                    string.Join(",", request.ProductIds), 
                    request.DesiredResults.Value);

                result.RecommendationSet = itemSets.RecommendedItemSetInfo;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}