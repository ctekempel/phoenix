﻿namespace Phoenix.Recommendations.Pipelines.Models
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class GetModels
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (GetModelsRequest)args.Request;
            var result = (GetModelsResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var modelService = new ModelService(accountKey, recommendationsServiceEndpoint);
                var models = modelService.GetAllModels();

                result.ModelList = models;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}