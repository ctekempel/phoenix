﻿namespace Phoenix.Recommendations.Pipelines.Models
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class CreateModel
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (CreateModelRequest)args.Request;
            Assert.ArgumentNotNullOrEmpty(request.ModelName, "args.request.ModelName");

            var result = (CreateModelResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var modelService = new ModelService(accountKey, recommendationsServiceEndpoint);
                var model = modelService.CreateModel(request.ModelName, request.ModelDescription);

                result.Model = model;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}