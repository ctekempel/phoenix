﻿namespace Phoenix.Recommendations.Pipelines.Training
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.Model.Build;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class TrainItemToItemModel
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (TrainItemToItemModelRequest)args.Request;
            Assert.ArgumentNotNull(request.Model, "args.request.Model");

            var result = (TrainItemToItemModelResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var trainingService = new TrainingService(accountKey, recommendationsServiceEndpoint);

                string operationLocationHeader;

                var buildId = trainingService.CreateItemToItemBuild(
                    request.Model.Id,
                    "Frequenty-Consumed-Together Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                    false,
                    out operationLocationHeader);

                var buildInfo =
                    trainingService.WaitForOperationCompletion<BuildInfo>(
                        TrainingService.GetOperationId(operationLocationHeader));

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}