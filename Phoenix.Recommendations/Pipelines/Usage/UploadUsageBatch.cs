﻿namespace Phoenix.Recommendations.Pipelines.Usage
{
    using System;

    using Phoenix.Foundation.ServiceProvider;
    using Phoenix.Recommendations.ServiceProvider;
    using Phoenix.Recommendations.Services;

    using Sitecore.Diagnostics;

    public class UploadUsageBatch
    {
        #region Public Methods and Operators

        public void Process(PhoenixServicePipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.Request, "args.request");

            var request = (UploadUsageBatchRequest)args.Request;
            Assert.ArgumentNotNull(request.Model, "args.request.Model");

            var result = (UploadUsageBatchResult)args.Result;

            try
            {
                var accountKey = Foundation.Settings.AzureCognitiveServicesAccountKey;
                var recommendationsServiceEndpoint = Settings.RecommendationsServiceEndpoint;

                var trainingService = new TrainingService(accountKey, recommendationsServiceEndpoint);

                var stats = trainingService.UploadUsage(request.Model.Id, request.UsageFilePath, request.UsageDisplayName);

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
        }

        #endregion
    }
}