﻿namespace Phoenix.Foundation.Pipelines
{
    using Sitecore.Pipelines;

    public class PhoenixPipelineService
    {
        #region Static Fields

        /// <summary>
        /// The default pipeline service.
        /// 
        /// </summary>
        public static readonly PhoenixPipelineService Default = new PhoenixPipelineService();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Runs the pipeline.
        /// 
        /// </summary>
        /// <typeparam name="TArgs">The type of the args.</typeparam><param name="pipelineName">Name of the pipeline.</param><param name="args">The args.</param>
        public virtual void RunPipeline<TArgs>(string pipelineName, TArgs args) where TArgs : PipelineArgs
        {
            CorePipeline.Run(pipelineName, (PipelineArgs)args);
        }

        #endregion
    }
}