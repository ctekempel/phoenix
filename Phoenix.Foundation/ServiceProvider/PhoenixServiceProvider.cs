﻿namespace Phoenix.Foundation.ServiceProvider
{
    using System;

    using Phoenix.Foundation.Pipelines;

    using Sitecore.Diagnostics;

    public abstract class PhoenixServiceProvider
    {
        #region Fields

        /// <summary>
        /// The pipeline service
        /// 
        /// </summary>
        private PhoenixPipelineService pipelineService;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the pipeline service.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The pipeline service.
        /// 
        /// </value>
        public PhoenixPipelineService PipelineService
        {
            get
            {
                return this.pipelineService ?? PhoenixPipelineService.Default;
            }

            set
            {
                Assert.ArgumentNotNull(value, "value");
                this.pipelineService = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Runs the pipeline.
        /// 
        /// </summary>
        /// <typeparam name="TRequest">The type of the request.</typeparam><typeparam name="TResult">The type of the result.</typeparam><param name="pipelineName">Name of the pipeline.</param><param name="request">The request.</param>
        /// <returns>
        /// The result.
        /// </returns>
        public virtual TResult RunPipeline<TRequest, TResult>(string pipelineName, TRequest request)
            where TRequest : PhoenixServiceProviderRequest where TResult : PhoenixServiceProviderResult, new()
        {
            var args = new PhoenixServicePipelineArgs(request, Activator.CreateInstance<TResult>());
            this.PipelineService.RunPipeline(pipelineName, args);
            return (TResult)args.Result;
        }

        #endregion
    }
}