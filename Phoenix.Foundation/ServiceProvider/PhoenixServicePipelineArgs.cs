namespace Phoenix.Foundation.ServiceProvider
{
    using Sitecore.Pipelines;

    public class PhoenixServicePipelineArgs : PipelineArgs
    {
        #region Fields

        private readonly PhoenixServiceProviderRequest request;

        private readonly PhoenixServiceProviderResult result;

        #endregion

        #region Constructors and Destructors

        public PhoenixServicePipelineArgs(
            PhoenixServiceProviderRequest request, 
            PhoenixServiceProviderResult result)
        {
            this.request = request;
            this.result = result;
        }

        #endregion

        #region Public Properties

        public PhoenixServiceProviderResult Result
        {
            get
            {
                return this.result;
            }
        }


        /// <summary>
        /// Gets the request.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The request.
        /// </value>
        public PhoenixServiceProviderRequest Request
        {
            get
            {
                return this.request;
            }
        }
        #endregion
    }
}