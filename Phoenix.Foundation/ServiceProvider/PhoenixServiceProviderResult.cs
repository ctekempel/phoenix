namespace Phoenix.Foundation.ServiceProvider
{
    using System;

    public abstract class PhoenixServiceProviderResult
    {
        public bool Success { get; set; }

        public Exception Exception { get; set; }
    }
}