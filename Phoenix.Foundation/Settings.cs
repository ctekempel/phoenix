namespace Phoenix.Foundation
{
    public static class Settings
    {
        private const string Azurecognitiveservicesaccountkey = "Phoenix.AzureCognitiveServicesAccountKey";

        private const string Azurecognitiveservicesendpoint = "Phoenix.AzureCognitiveServicesEndpoint";

        #region Public Properties

        public static string AzureCognitiveServicesAccountKey
        {
            get
            {
                return Sitecore.Configuration.Settings.GetSetting(Azurecognitiveservicesaccountkey);
            }
        }

        public static string AzureCognitiveServicesEndpoint
        {
            get
            {
                return Sitecore.Configuration.Settings.GetSetting(Azurecognitiveservicesendpoint);
            }
        }

        #endregion
    }
}